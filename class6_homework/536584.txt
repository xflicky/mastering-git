# Question 1
git log --oneline --merges --before='2023-10-30' --after='2023-09-01' | wc -l

There 58 commits.
---
# Question 2
git log --grep "Merge branch 'hw03' into 'main'"

Commit body:
    Merge branch 'hw03' into 'main'
    
    Hw03
    
    See merge request redhat/research/mastering-git!86

Commit author: Tomas Tomecek <ttomecek@redhat.com>
---
# Question 3
git log --oneline

Commit: 38d3aca 
Title: apply Tom's MR suggestions
Recommendation: Commit title should tell me more about what was done. I dont really care if it
		 was a suggestion, who was author of the suggestion. Important is how the suggestion affect project.
---
# Question 4
git log --since="2023-09-01" --until="2023-09-30" --name-status

commit 9f1b38a5ecec2b6e6633f77e14bc902ffc583243
Author: Tomas Tomecek <ttomecek@redhat.com>
Date:   Mon Jul 31 15:35:21 2023 +0200

    add labs 1 2 3
    
    Signed-off-by: Tomas Tomecek <ttomecek@redhat.com>
---
# Question 5
git shortlog --since 1.9.2023 --until 31.10.2023 -s | wc -l

Number of unique contributors is 44.
---
# Question 6
git rev-list main...main-oct-9 | wc -l

119 commits differ between main and main-oct-9. 
---
# Question 7
git log -S "**Grading**: successfully complete 5 mandatory homeworks, or 4/5 homeworks plus a bonus task"

commit 35c747bd17c2dab94d4331a9fcfbc6dd53dfbec8
Author: Irina Gulina <igulina@redhat.com>
Date:   Mon Sep 25 10:28:16 2023 +0000

    correct a link syntax and HW info in Readme

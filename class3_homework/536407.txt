This course has been pretty helpful so far. 
I like that the instructors have hands-on experience with Git in their jobs and they seem passionate about it. 
I also enjoy having homework to practice what we learned.
